require 'test_helper'

class RoadTransportsControllerTest < ActionController::TestCase
  setup do
    @road_transport = road_transports(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:road_transports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create road_transport" do
    assert_difference('RoadTransport.count') do
      post :create, road_transport: { code: @road_transport.code, frequency: @road_transport.frequency, map: @road_transport.map, name: @road_transport.name, population: @road_transport.population, schedule: @road_transport.schedule, station_quantity: @road_transport.station_quantity, timelapse: @road_transport.timelapse, units: @road_transport.units, users_quantity: @road_transport.users_quantity }
    end

    assert_redirected_to road_transport_path(assigns(:road_transport))
  end

  test "should show road_transport" do
    get :show, id: @road_transport
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @road_transport
    assert_response :success
  end

  test "should update road_transport" do
    patch :update, id: @road_transport, road_transport: { code: @road_transport.code, frequency: @road_transport.frequency, map: @road_transport.map, name: @road_transport.name, population: @road_transport.population, schedule: @road_transport.schedule, station_quantity: @road_transport.station_quantity, timelapse: @road_transport.timelapse, units: @road_transport.units, users_quantity: @road_transport.users_quantity }
    assert_redirected_to road_transport_path(assigns(:road_transport))
  end

  test "should destroy road_transport" do
    assert_difference('RoadTransport.count', -1) do
      delete :destroy, id: @road_transport
    end

    assert_redirected_to road_transports_path
  end
end
