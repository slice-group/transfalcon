require 'test_helper'

class RiffilsCentersControllerTest < ActionController::TestCase
  setup do
    @riffils_center = riffils_centers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:riffils_centers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create riffils_center" do
    assert_difference('RiffilsCenter.count') do
      post :create, riffils_center: { address: @riffils_center.address, map: @riffils_center.map, municipality_id: @riffils_center.municipality_id, name: @riffils_center.name, phone: @riffils_center.phone }
    end

    assert_redirected_to riffils_center_path(assigns(:riffils_center))
  end

  test "should show riffils_center" do
    get :show, id: @riffils_center
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @riffils_center
    assert_response :success
  end

  test "should update riffils_center" do
    patch :update, id: @riffils_center, riffils_center: { address: @riffils_center.address, map: @riffils_center.map, municipality_id: @riffils_center.municipality_id, name: @riffils_center.name, phone: @riffils_center.phone }
    assert_redirected_to riffils_center_path(assigns(:riffils_center))
  end

  test "should destroy riffils_center" do
    assert_difference('RiffilsCenter.count', -1) do
      delete :destroy, id: @riffils_center
    end

    assert_redirected_to riffils_centers_path
  end
end
