#Generado con Keppler.
class RoadTransportsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_road_transport, only: [:show, :edit, :update, :destroy]
  before_action :set_message

  # GET /road_transports
  def index
    road_transports = RoadTransport.searching(@query).all
    @objects, @total = road_transports.page(@current_page), road_transports.size
    redirect_to road_transports_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /road_transports/1
  def show
  end

  # GET /road_transports/new
  def new
    @road_transport = RoadTransport.new
  end

  # GET /road_transports/1/edit
  def edit
    @sectors = Sector.where(municipality_id: @road_transport.municipality_id)
  end

  # POST /road_transports
  def create
    @road_transport = RoadTransport.new(road_transport_params)

    if @road_transport.save
      redirect_to @road_transport, notice: 'Road transport was successfully created.'
    else
      @sectors = Sector.where(municipality_id: @road_transport.municipality_id)
      render :new
    end
  end

  # PATCH/PUT /road_transports/1
  def update
    if @road_transport.update(road_transport_params)
      redirect_to @road_transport, notice: 'Road transport was successfully updated.'
    else
      @sectors = Sector.where(municipality_id: @road_transport.municipality_id)
      render :edit
    end
  end

  # DELETE /road_transports/1
  def destroy
    @road_transport.destroy
    redirect_to road_transports_url, notice: 'Road transport was successfully destroyed.'
  end

  def destroy_multiple
    RoadTransport.destroy redefine_ids(params[:multiple_ids])
    redirect_to road_transports_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_road_transport
      @road_transport = RoadTransport.find(params[:id])
      @municipalities = Municipality.all
    end

    def set_message
      @message = KepplerContactUs::Message.new
    end

    # Only allow a trusted parameter "white list" through.
    def road_transport_params
      params.require(:road_transport).permit(:code, :name, :schedule, :units, :frequency, :users_quantity, :population, :station_quantity, :timelapse, :map, :municipality_id, :sector_ids => [])
    end
end
