#Generado con Keppler.
class RiffilsCentersController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_riffils_center, only: [:show, :edit, :update, :destroy]

  # GET /riffils_centers
  def index
    riffils_centers = RiffilsCenter.searching(@query).all
    @objects, @total = riffils_centers.page(@current_page), riffils_centers.size
    redirect_to riffils_centers_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /riffils_centers/1
  def show
  end

  # GET /riffils_centers/new
  def new
    @riffils_center = RiffilsCenter.new
  end

  # GET /riffils_centers/1/edit
  def edit
  end

  # POST /riffils_centers
  def create
    @riffils_center = RiffilsCenter.new(riffils_center_params)

    if @riffils_center.save
      redirect_to @riffils_center, notice: 'Riffils center was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /riffils_centers/1
  def update
    if @riffils_center.update(riffils_center_params)
      redirect_to @riffils_center, notice: 'Riffils center was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /riffils_centers/1
  def destroy
    @riffils_center.destroy
    redirect_to riffils_centers_url, notice: 'Riffils center was successfully destroyed.'
  end

  def destroy_multiple
    RiffilsCenter.destroy redefine_ids(params[:multiple_ids])
    redirect_to riffils_centers_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_riffils_center
    @riffils_center = RiffilsCenter.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def riffils_center_params
    params.require(:riffils_center).permit(:name, :address, :municipality_id, :schedule, :phone, :map, :sector_ids => [])
  end
end
