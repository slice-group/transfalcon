class FrontendController < ApplicationController
  layout 'layouts/frontend/application'
  before_filter :selector, only: [:get_sectors, :get_center]
  before_filter :form_message

  def index
    @posts = KepplerBlog::Post.where(public: true).last(4)
    @municipality = Municipality.all.order('name ASC')
    @catalog_fotos = KepplerCatalogs::Catalog.find_by_section('Galeria')
    @catalog = KepplerCatalogs::Catalog.find_by_name('FOTOS')
    @attachments = @catalog_fotos.attachments.where(public: true).last(4) if @catalog_fotos
  end

  def option_landing_gallery
    @catalog = KepplerCatalogs::Catalog.find_by_name(params[:option])
    @attachments = @catalog.attachments.where(public: true).last(4)
  end

  def show_gallery
    @catalog = KepplerCatalogs::Catalog.find_by_name(params[:option])
    @attachments = @catalog.attachments.where(public: true).order(created_at: :desc)
    @attachments = @attachments.page(params[:page]).per(16)
  end

  def option_show_gallery
    show_gallery
  end

  def get_sectors
    @municipality = Municipality.find(params[:municipality_id])
    respond_to do |format|
      format.js {}
    end
  end

  def get_center
    @sector = Sector.find(params[:center_id])
    respond_to do |format|
      format.js {}
    end
  end

  def get_option
    case params[:commit]
    when 'center'
      @nunicipality = Municipality.find(params[:municipality])
      @sector = Sector.find(params[:sector])
      @riffils_center = RiffilsCenter.find(params[:center])
      redirect_to show_center_path(@nunicipality.permalink, @sector.permalink, @riffils_center.permalink, :anchor => "anchor")
    when 'road'
      @nunicipality = Municipality.find(params[:municipality])
      @sector = Sector.find(params[:sector])
      @road_transport = RoadTransport.find(params[:road])
      redirect_to show_road_path(@nunicipality.permalink, @sector.permalink, @road_transport.permalink, :anchor => "anchor")
    end
  end

  def show_center
    @municipality = Municipality.all
    @municipality_load = Municipality.find_by_permalink(params[:municipality_permalink])
    @sector = Sector.find_by_permalink(params[:sector_permalink])
    @riffils_center = RiffilsCenter.find_by_permalink(params[:riffils_center_permalink])
  end

  def show_road
    @municipality = Municipality.all
    @municipality_load = Municipality.find_by_permalink(params[:municipality_permalink])
    @sector = Sector.find_by_permalink(params[:sector_permalink])
    @road_transport = RoadTransport.find_by_permalink(params[:road_transport_permalink])
  end


  private

  def form_message
    @message = KepplerContactUs::Message.new
  end

  def selector
    @selector = case params[:opcion]
    when "center"
      "center"
    when "road"
      "road"
    end
  end
end
