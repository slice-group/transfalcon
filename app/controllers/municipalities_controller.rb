#Generado con Keppler.
class MunicipalitiesController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_municipality, only: [:show, :edit, :update, :destroy]

  # GET /municipalities
  def index
    municipalities = Municipality.searching(@query).all
    @objects, @total = municipalities.page(@current_page), municipalities.size
    redirect_to municipalities_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /municipalities/1
  def show
  end

  # GET /municipalities/new
  def new
    @municipality = Municipality.new
  end

  # GET /municipalities/1/edit
  def edit
  end

  # POST /municipalities
  def create
    @municipality = Municipality.new(municipality_params)

    if @municipality.save
      redirect_to @municipality, notice: 'Municipality was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /municipalities/1
  def update
    if @municipality.update(municipality_params)
      redirect_to @municipality, notice: 'Municipality was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /municipalities/1
  def destroy
    @municipality.destroy
    redirect_to municipalities_url, notice: 'Municipality was successfully destroyed.'
  end

  def destroy_multiple
    Municipality.destroy redefine_ids(params[:multiple_ids])
    redirect_to municipalities_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_municipality
      @municipality = Municipality.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def municipality_params
      params.require(:municipality).permit(:name)
    end
end
