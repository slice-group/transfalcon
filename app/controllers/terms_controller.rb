#Generado con Keppler.
class TermsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_term, only: [:show, :edit, :update, :destroy, :view_client_terms]

  # GET /terms
  def index
    terms = Term.searching(@query).all
    @objects, @total = terms.page(@current_page), terms.size
    redirect_to terms_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /terms/1
  def show
  end

  # GET /terms/new
  def new
    @term = Term.new
  end

  # GET /terms/1/edit
  def edit
  end

  # POST /terms
  def create
    @term = Term.new(term_params)

    if @term.save
      redirect_to @term, notice: 'Term was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /terms/1
  def update
    if @term.update(term_params)
      redirect_to @term, notice: 'Term was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /terms/1
  def destroy
    @term.destroy
    redirect_to terms_url, notice: 'Term was successfully destroyed.'
  end

  def destroy_multiple
    Term.destroy redefine_ids(params[:multiple_ids])
    redirect_to terms_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_term
      @term = Term.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def term_params
      params.require(:term).permit(:description)
    end
end
