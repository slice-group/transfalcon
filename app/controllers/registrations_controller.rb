class RegistrationsController < Devise::RegistrationsController

	before_action :set_terms, only: [:new, :create]

  protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def set_terms
  	@term = Term.last
  end
end