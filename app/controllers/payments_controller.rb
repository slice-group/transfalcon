#Generado con Keppler.
class PaymentsController < ApplicationController  
  before_filter :authenticate_user!
  layout :resolve_layout
  load_and_authorize_resource
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  # GET /payments
  def index
    payments = Payment.searching(@query).all
    @objects, @total = payments.page(@current_page), payments.size
    redirect_to payments_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /payments/1
  def show
  end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit
  end

  # POST /payments
  def create
    @payment = Payment.new(payment_params)

    if @payment.save
      redirect_to @payment, notice: 'Payment was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /payments/1
  def update
    if @payment.update(payment_params)
      redirect_to @payment, notice: 'Payment was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /payments/1
  def destroy
    @payment.destroy
    redirect_to payments_url, notice: 'Payment was successfully destroyed.'
  end

  def destroy_multiple
    Payment.destroy redefine_ids(params[:multiple_ids])
    redirect_to payments_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def payment_params
      params.require(:payment).permit(:mp_code, :transfer_code, :transfer_date, :transfer_account, :transfer_bank, :type_payment)
    end

    def resolve_layout
    case action_name.to_sym
    when :update_user_edit, :edit_profile
      'frontend/application'
    else
      'admin/application'
    end
  end
end
