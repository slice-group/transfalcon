class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout :layout_by_resource
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :get_paginator_params
  before_action :can_multiple_destroy, only: [:destroy_multiple]
  before_filter :selector, only: [:sectors_of_municipality]
  before_filter :form_message

  rescue_from CanCan::AccessDenied do |exception|
    exception.default_message = exception.action.eql?(:index) ? "No estás autorizado para acceder a esta página" : "No estás autorizado para realizar esta acción"
    redirect_to not_authorized_path, flash: { message: exception.message }
  end

  def after_sign_in_path_for(resource)
    if current_user.rol.eql?('admin') || current_user.rol.eql?('superadmin')
      admin_index_path
    elsif current_user.rol.eql?('client')
      view_orders_path
    else
      root_path
    end
  end

  def new
    @term = Term.last
    build_resource({})
    yield resource if block_given?
    respond_with resource
  end

  # POST /resource
  def create
    @term = Term.last
    build_resource(sign_up_params)

    resource.save
    yield resource if block_given?
    if resource.persisted?
      if resource.active_for_authentication?
        set_flash_message! :notice, :signed_up
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      set_minimum_password_length
      respond_with resource
    end
  end

  def get_paginator_params
    @query = (params[:search] and !params[:search].blank?) ? params[:search] : nil
    @current_page = (params[:page] and !params[:page].blank?) ? params[:page] : nil
  end

  # GET sectors of minicipality only RoadTransportsController and RefillsCentersController
  def sectors_of_municipality
    @municipality = Municipality.find(params[:municipality_id])
    respond_to do |format|
       format.js {  }
    end
  end

  private

  def redefine_ids(ids)
    ids.delete("[]").split(",").select { |id| id if controller_path.classify.constantize.exists? id }
  end
  
  # verificar si el usuario tiene permisos para eliminar cada uno de los objects seleccionados
  def can_multiple_destroy
    redefine_ids(params[:multiple_ids]).each do |id|
      authorize! :destroy, controller_path.classify.constantize.find(id)
    end
  end

  def selector
    @selector = case controller_name
    when "riffils_centers"
      ".riffils_center_sector_ids"
    when "road_transports"
      ".road_transport_sector_ids"
    end
  end

  protected
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :email, :password, :password_confirmation, :usertype, :identification, :address, :phone1, :phone2, :contact)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:name, :email, :password, :password_confirmation, :current_password, :usertype, :identification, :address, :phone1, :phone2, :contact)
    end
  end

  def layout_by_resource
    if devise_controller? 
      "frontend/application"
    else
      "application"
    end
  end

  def form_message
    @message = KepplerContactUs::Message.new
  end

end
