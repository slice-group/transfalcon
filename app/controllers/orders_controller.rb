#Generado con Keppler.
class OrdersController < ApplicationController  
  before_filter :authenticate_user!
  layout :resolve_layout
  load_and_authorize_resource
  before_action :set_order, only: [:show, :edit, :update, :destroy, :validate_stock, :change_status]
  before_action :validate_stock, only: :create_orders

  # GET /orders
  def index
    orders = Order.searching(@query).all
    @objects, @total = orders.page(@current_page), orders.size
    redirect_to orders_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /orders/1
  def show
  end

  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders
  def create
    @order = Order.new(order_params)

    if @order.save
      redirect_to orders_path, notice: 'Order was successfully created.'
    else
      render :new
    end
  end

  #Post /orders Clients
  def create_orders
    @order = Order.new(order_params)
    @order.user_id = current_user.id
    @order.product_id = Product.first.id

    if @order.save
      @order.generate_code
      if @order.transfer?
        redirect_to view_orders_path, notice: 'La orden ha Sido Creada con Éxito'
      else
        redirect_to @order.url_mercadopago
      end
    else
      build_variables
      render "orders/view_orders", error: 'La Orden no ha Sido Creada, Vuelve a Intentar'
    end
  end

  # PATCH/PUT /orders/1
  def update
    if @order.update(order_params)
      redirect_to @order, notice: 'Tu Orden ha Sido Actualizada con Éxito'
    else
      render :edit
    end
  end

  # DELETE /orders/1
  def destroy
    @order.destroy
    redirect_to orders_url, notice: 'La Orden ha Sido Eliminada con Exito'
  end

  def destroy_multiple
    Order.destroy redefine_ids(params[:multiple_ids])
    redirect_to orders_path(page: @current_page, search: @query), notice: 'Usuarios eliminados satisfactoriamente'
  end

  def view_orders
    build_variables
    @order = Order.new(price: @product.price, quantity: 1)
    @order.build_payment
  end

  def send_receipt
    OrderMailer.send_order(Order.find(params[:id])).deliver
    redirect_to :back, notice: 'Tu Recibo ha Sido Enviado con Éxito'
  end

  def send_status_receipt
    StatusMailer.status_order(Order.find(params[:id])).deliver
  end

  def change_status
    if @order.update(order_params)
      check_quantity
      if @order.status.eql?('Pendiente')
        redirect_to :back, notice: 'Tu Orden No ha Sido Actualizada por falta de Stock'
      else
        redirect_to :back, notice: 'Tu Orden ha Sido Actualizada con Éxito'
        send_status_receipt
      end
    else
      redirect_to :back, notice: 'Tu Orden No ha Sido Actualizada, Vuelve a Intentar'
    end
  end

  def validate_payment
    @order = Order.find(params[:id])
    if @order.in_stock?
      redirect_to @order.url_mercadopago
    else
      redirect_to :back, danger: 'No hay productos en este momento'
    end
  end

  private

    def check_quantity
      if @order.status.eql?('Aprobada') && @order.quantity <= @order.product.quantity
        @order.stock_out
      elsif @order.status.eql?('Rechazada')
        @order.update_attribute(:status, 'Rechazada')
      else
        @order.update_attribute(:status, 'Pendiente')
      end
    end

    def set_order
      @order = Order.find(params[:id])
    end

    def build_variables
      @product = Product.first
      @orders = Order.where(user_id: current_user.id).order('created_at desc')
    end

    def validate_stock
      @order = Order.new(order_params)
      @order.product_id = Product.first.id
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:quantity, :price, :code, :status, :product_id, payment_attributes: [:mp_code, :type_payment, :transfer_code, :transfer_date, :transfer_bank, :transfer_account])
    end

    def resolve_layout
    case action_name.to_sym
    when :view_orders, :create_orders
      'frontend/application'
    else
      'admin/application'
    end
  end

end
