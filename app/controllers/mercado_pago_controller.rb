class MercadoPagoController < ApplicationController

  protect_from_forgery except: [:create, :hook]
  before_action :set_params_success, only: :success
  before_action :set_params_hook, only: :hook

  def success
    if @status && !payment_exist?
      @order.payment.update(mp_code: @mp_code)
      @order.update(status: "Aprobada")
    end
    redirect_to view_orders_path, notice: "Su pago ha sido procesado"
  end

  def failure
    redirect_to view_orders_path, error: "Su pago no ha sido procesado, intente nuevamente"    
  end

  def hook
    if @status && !payment_exist?
      @order.payment.update(mp_code: @mp_code)
      @order.update(status: "Aprobada")
    end

    render nothing: true
  end

  private

  def payment_exist?
    Payment.find_by_mp_code(@mp_code)
  end

  def set_params_hook
    if params[:topic].eql?('payment')
      payment = MercadoPagoGateway::MP.notification(params['id'])
      puts payment
      @order = Order.find_by_code(payment['collection']['external_reference'])
      @status = payment['collection']["status"].eql?("approved")
      @mp_code = payment['collection']['id']
    end
  end

  def set_params_success
    if params['collection_status'] && params['collection_status'].eql?('approved')
      @order = Order.find_by_code(params['external_reference'])
      @status = params['collection_status'].eql?("approved")
      @mp_code = params['collection_id']
    end
  end

end