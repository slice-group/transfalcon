#Generado con Keppler.
class SectorsController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_sector, only: [:show, :edit, :update, :destroy]
  before_action :set_message

  # GET /sectors
  def index
    sectors = Sector.searching(@query).all
    @objects, @total = sectors.page(@current_page), sectors.size
    redirect_to sectors_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /sectors/1
  def show
  end

  # GET /sectors/new
  def new
    @sector = Sector.new
  end

  # GET /sectors/1/edit
  def edit
  end

  # POST /sectors
  def create
    @sector = Sector.new(sector_params)

    if @sector.save
      redirect_to @sector, notice: 'Sector was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /sectors/1
  def update
    if @sector.update(sector_params)
      redirect_to @sector, notice: 'Sector was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /sectors/1
  def destroy
    @sector.destroy
    redirect_to sectors_url, notice: 'Sector was successfully destroyed.'
  end

  def destroy_multiple
    Sector.destroy redefine_ids(params[:multiple_ids])
    redirect_to sectors_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sector
      @sector = Sector.find(params[:id])
    end

    def set_message
      @message = KepplerContactUs::Message.new
    end

    # Only allow a trusted parameter "white list" through.
    def sector_params
      params.require(:sector).permit(:name, :municipality_id)
    end
end
