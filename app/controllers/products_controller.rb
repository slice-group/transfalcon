#Generado con Keppler.
class ProductsController < ApplicationController  
  before_filter :authenticate_user!
  layout :resolve_layout
  load_and_authorize_resource
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  def index
    products = Product.searching(@query).all
    @objects, @total = products.page(@current_page), products.size
    redirect_to products_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /products/1
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Product was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
    redirect_to products_url, notice: 'Product was successfully destroyed.'
  end

  def destroy_multiple
    Product.destroy redefine_ids(params[:multiple_ids])
    redirect_to products_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:name, :quantity, :price)
    end

    def resolve_layout
    case action_name.to_sym
    when :update_user_edit, :edit_profile
      'frontend/application'
    else
      'admin/application'
    end
  end
end
