class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.has_role? :admin

      # - user authorize -
      can [:delete, :show, :edit, :update, :create, :index, :export, :destroy_multiple, :order], User
      can :destroy, User do |u| !u.eql?(user) end

      # - blog authorize -
      can :manage, KepplerBlog::Post
      can :manage, KepplerBlog::Category
      can :manage, RoadTransport
      can :manage, Municipality
      can :manage, Sector
      can :manage, RiffilsCenter
      can :manage, KepplerContactUs::Message
      can :manage, User
      can [:index, :show, :edit, :update], Term
      can [:change_status, :view_orders, :index, :show, :create_orders, :send_receipt, :validate_payment], Order
      can [:index, :show, :edit, :update], Product
      can [:index, :show, :edit, :update], Payment
      can :manage, KepplerCatalogs::Catalog
      can :manage, KepplerCatalogs::Category
      can :manage, KepplerCatalogs::Attachment

    elsif user.has_role? :superadmin

      # - user authorize -
      can [:delete, :show, :edit, :update, :create, :index, :export, :destroy_multiple], User
      can :destroy, User do |u| !u.eql?(user) end

      can [:update_user_edit, :edit_profile], User
      can [:change_status, :view_orders, :index, :show, :create_orders, :send_receipt, :validate_payment], Order

      # - blog authorize -
      can :manage, KepplerBlog::Post
      can :manage, KepplerBlog::Category
      can :manage, RoadTransport
      can :manage, Municipality
      can :manage, Sector
      can :manage, RiffilsCenter      
      can [:index, :show, :edit, :update], Term
      can [:index, :show, :edit, :update], Product
      can [:index, :show, :edit, :update], Payment
      can :manage, KepplerContactUs::Message
      can :manage, KepplerCatalogs::Catalog
      can :manage, KepplerCatalogs::Category
      can :manage, KepplerCatalogs::Attachment

    elsif user.has_role? :operador

      # - blog authorize -
      can :manage, RoadTransport
      can :manage, Municipality
      can :manage, Sector
      can :manage, RiffilsCenter
      
    elsif user.has_role? :autor
      
      can :manage, KepplerBlog::Category
      # - user authorize -
      can [:edit, :update], User, :id => user.id

      # - blog authorize -
      can :manage, KepplerBlog::Post, :user_id => user.id
      can :manage, KepplerBlog::Category
      

    elsif user.has_role? :editor

      #- user authorize -
      can [:edit, :update], User, :id => user.id

      # - blog authorize -
      can [:index, :update, :edit, :show], KepplerBlog::Post
    
    elsif user.has_role? :client
      # - user authorize -
      can [:update_user_edit, :edit_profile], User
      can [:view_orders, :update, :edit, :create, :index, :delete, :destroy_multiple, :create_orders, :send_receipt, :validate_payment], Order
    end
  end
end
