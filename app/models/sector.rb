#Generado por keppler
require 'elasticsearch/model'
class Sector < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  belongs_to :municipality
  has_and_belongs_to_many :road_transports
  has_and_belongs_to_many :riffils_centers
  validates_presence_of :municipality, :name
  
  
  after_commit on: [:update] do
    __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order('name ASC')
    else
      self.order('name ASC')
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :name, :municipality] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name,
      municipality: self.municipality.name
    }.as_json
  end

  private

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

end
#Sector.import
