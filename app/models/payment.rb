#Generado por keppler
require 'elasticsearch/model'
class Payment < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :order, inverse_of: :payment

  validates_presence_of :type_payment
  validates_presence_of :transfer_bank, if: :transfer?
  validates_presence_of :transfer_code, if: :transfer?

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def transfer?
    type_payment.eql?('Transferencia Bancaria')
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      mp_code:  self.mp_code.to_s,
      transfer_code:  self.transfer_code.to_s,
      transfer_date:  self.transfer_date,
      transfer_bank:  self.transfer_bank.to_s,
      order:  self.order.to_s,
    }.as_json
  end

end
#Payment.import
