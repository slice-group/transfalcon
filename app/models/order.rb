#Generado por keppler
require 'elasticsearch/model'
class Order < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_one :payment
  belongs_to :user
  belongs_to :product
  accepts_nested_attributes_for :payment, allow_destroy: true
  accepts_nested_attributes_for :user

  validates_presence_of :quantity
  validates_associated :payment
  validate :quantity_more_that_stock?, :quantity_zero?, :validate_price?

  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end

  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  def generate_code
    limit = 999_999
    self.code = self.id 
    (1..(limit.to_s.size - self.code.to_s.size)).each do
      self.code = '0' << self.code.to_s
    end if self.id < limit
    update_attribute(:code, self.code)
  end

  def stock_out
    stock = self.product.quantity - self.quantity
    self.product.update_attribute(:quantity, stock)
  end

  def in_stock?
    self.quantity <= self.product.quantity
  end

  def quantity_zero?
    errors.add(:quantity, 'No puede ser cero') if quantity.nil? || quantity.zero?
  end

  def quantity_more_that_stock?
    if (quantity.nil? || quantity > product.quantity) && !created_at?
      errors.add(:quantity, 'no hay disponibilidad, intente nuevamente')
    end
  end

  def subtotal
    quantity * product.price
  end

  def transfer?
    payment.type_payment.eql?('Transferencia Bancaria')
  end

  def validate_price?
    errors.add(:price, 'no coincide con el del producto') if subtotal != price && !created_at?
  end

  def url_mercadopago
    preference = MercadoPagoGateway::MP.create_preference(data)
    preference['init_point']
  end
  # armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      quantity:  self.quantity.to_s,
      price:  self.price.to_s,
      user:  self.user.to_s,
      code:  self.code.to_s,
      status:  self.status.to_s,
      product:  self.product.to_s,
    }.as_json
  end

  private

    def data
      {
        external_reference: code,
        items: [
          {
            id: id,
            title: "Order number #{code}",
            quantity: quantity,
            unit_price: product.price,
            currency_id: 'VEF'
          }
        ],
        payer: {
          email: user.email,
          name: user.name,
          identification: {
            number: user.identification
          }
        },
        payment_methods: {
          excluded_payment_types: [{ id: 'atm' }, { id: 'ticket' }]
        },
        back_urls: {
          success: "http://www.transfalcon.com.ve/ordenes/success",
          failure: "http://www.transfalcon.com.ve/ordenes/failure"
        },
        notification_url: "http://www.transfalcon.com.ve/ordenes/hook"
      }
    end
end
#Order.import
