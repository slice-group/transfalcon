#Generado por keppler
require 'elasticsearch/model'
class RiffilsCenter < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :municipality
  before_save :create_permalink, on: :create
  has_and_belongs_to_many :sectors
  
  after_commit on: [:update] do
    __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :name, :address, :municipality] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name:  self.name,
      address:  self.address,
      municipality:  self.municipality.name,
    }.as_json
  end

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

end
#RiffilsCenter.import
