#Generado por keppler
require 'elasticsearch/model'
class RoadTransport < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  before_save :create_permalink, on: :create
  belongs_to :municipality
  has_and_belongs_to_many :sectors
  validates_presence_of :code, :name, :schedule, :units, :frequency, :users_quantity, :population, :station_quantity, :timelapse
  validates_uniqueness_of :code, :name

  after_commit on: [:update] do
    __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:code, :name, :schedule, :units, :frequency, :users_quantity, :population, :station_quantity, :timelapse, :permalink] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      code: self.code,
      name: self.name,
      municipality: self.municipality.name
    }.as_json
  end

  private

  def create_permalink
    self.permalink = self.name.downcase.parameterize    
  end

end
#RoadTransport.import
