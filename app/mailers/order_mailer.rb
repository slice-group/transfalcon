class OrderMailer < ApplicationMailer
    def send_order(order)
        @order = order
        mail(to: "#{order.user.email}, comercios@transfalcon.com.ve", subject: 'Correo Transfalcon')
    end
end