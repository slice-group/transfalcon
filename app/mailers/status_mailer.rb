class StatusMailer < ApplicationMailer
    def status_order(order)
        @order = order
        mail(to: "#{order.user.email}, comercios@transfalcon.com.ve", subject: 'Correo Transfalcon')
    end
end