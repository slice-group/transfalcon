module FrontendHelper
  def header_section(title, subtitle)
    content_tag :div, class: "header-section" do
      content_tag(:div, title, class: "title-section")+
      content_tag(:div, image_tag("frontend/points.png"), class: "image-section")+
      content_tag(:div, subtitle,  class: "subtitle-section")
    end
  end

  def buttons_gallery_section(option, position)
    content_tag :div, class: "col-lg-6 col-xs-6 no-padding" do
      content_tag(
        :button, "#{option}",
        class: "btn btn-option-gallery pull-#{position}",
        id: "option-#{option.downcase}"
      )
    end
  end

  def root_path?
    controller_name.eql? "frontend" and action_name.eql? "index"
  end

  def show_blog?
    controller_name.eql? "blog" and action_name.eql? "show"
  end

  def listing_blog?
    controller_name.eql? "blog" and action_name.eql? "index"
  end

  def show_center?
    controller_name.eql? "frontend" and action_name.eql? "show_center"
  end

  def show_road?
    controller_name.eql? "frontend" and action_name.eql? "show_road"
  end

  def frontend_index?
    controller_name.eql?('frontend') && action_name.eql?('index')
  end

  def sign_in?
    controller_name.eql?('sessions') && action_name.eql?('new')
  end

  def option_landing_gallery?
    controller_name.eql?('frontend') && action_name.eql?('option_landing_gallery')
  end
end
