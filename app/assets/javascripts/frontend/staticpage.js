function removeHash () { 
  history.pushState("", document.title, window.location.pathname + window.location.search);
}

$( document ).ready(function() {

	$('#front-navbar').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 600,
    scrollOffset: 80,
    scrollThreshold: 0.5,
    filter: ':not(.external)',
    easing: 'swing',
    begin: function() {
      removeHash();

    },
    end: function() {
        $('.navbar-collapse').removeClass('in'); 

    },
    scrollChange: function($currentListItem) {
   
    }

  });
});

$( document ).ready(function() {

    $('#front-navbar-mobile').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 600,
    scrollOffset: 20,
    scrollThreshold: 0.5,
    filter: ':not(.external)',
    easing: 'swing',
    begin: function() {
      removeHash();

    },
    end: function() {
        $('.navbar-collapse').removeClass('in');

    },
    scrollChange: function($currentListItem) {

   
    }

  });
});









