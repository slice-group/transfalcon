$(document).on 'ready page:load', () ->
	$('#riffils_center_municipality_select').change ->
	  $.ajax
	    url: '/get-sectors/center/'+$('#riffils_center_municipality_select option:selected').val()
	    type: 'GET'
	  return

	$('#riffils_center_municipality_select2').change ->
	  $.ajax
	    url: '/get-sectors/road/'+$('#riffils_center_municipality_select2 option:selected').val()
	    type: 'GET'
	  return

	$('#riffils_center_sector_select').change ->
	  $.ajax
	    url: '/get-center/center/'+$('#riffils_center_sector_select option:selected').val()
	    type: 'GET'
	  return

	$('#riffils_center_sector_select2').change ->
	  $.ajax
	    url: '/get-center/road/'+$('#riffils_center_sector_select2 option:selected').val()
	    type: 'GET'
	  return

	$("#riffils_center_sector_center").change ->
		$("#btn-center").prop "disabled", false


	$("#riffils_center_sector_road").change ->
		$("#btn-road").prop "disabled", false
	return