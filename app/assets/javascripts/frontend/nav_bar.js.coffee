$(document).on 'ready page:load', () -> 
  unless window.location.hash is ""
    scroll =  parseInt($("#" + window.location.hash.substring(1)).offset().top-30)
    window.scrollTo 0, scroll

  $(window).bind "scroll", ->
    num = parseInt($("#home").css("height"))
    if $(window).scrollTop() >= num - (num*0.70)
      $("#front-navbar").css background: "#fff"
      $("#front-navbar .nav-link").css color: "black"
      $("#front-navbar").css "box-shadow": "0 0 4px rgba(0, 0, 0, .2)"
      $("#front-navbar .name-bar").css color: "black"
      $("#front-navbar .link-socials").hide()
      $("#front-navbar .logo-transfalcon-home").show()
    else
      $("#front-navbar").css background: "transparent"
      $("#front-navbar .name-bar").css color: "#fff"
      $("#front-navbar .nav-link").css color: "white"
      $("#front-navbar .link-socials").show()
      $("#front-navbar .logo-transfalcon-home").hide()
      $("#front-navbar").css "box-shadow": "none"
  
  $("#social-twitter").click ->
    url = "https://twitter.com/TRANSFALCON"
    window.open url, "_blank"

  $("#social-facebook").click ->
    url = "https://www.facebook.com/Transfalcon/?fref=ts"
    window.open url, "_blank"

  $("#social-instagram").click ->
    url = "https://www.instagram.com/TRANSFALCON"
    window.open url, "_blank"
    