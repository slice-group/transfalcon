#= require jquery
#= require jquery_ujs
#= require keppler_blog/frontend/application
#= require bootstrap
#= require bootstrap-datepicker
#= require bootstrap-datepicker/locales/bootstrap-datepicker.es.js
#= require_tree .