class AddTransferAccountToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :transfer_account, :string
  end
end
