class CreateRiffilsCenters < ActiveRecord::Migration
  def change
    create_table :riffils_centers do |t|
      t.string :name
      t.text :address
      t.integer :municipality_id
      t.string :permalink
      t.string :schedule
      t.string :phone
      t.text :map

      t.timestamps null: false
    end
  end
end
