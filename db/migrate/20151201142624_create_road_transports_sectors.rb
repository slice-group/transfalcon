class CreateRoadTransportsSectors < ActiveRecord::Migration
  def change
    create_table :road_transports_sectors do |t|
      t.integer :road_transport_id
      t.integer :sector_id

      t.timestamps null: false
    end
  end
end
