class CreateRiffilsCentersSectors < ActiveRecord::Migration
  def change
    create_table :riffils_centers_sectors do |t|
      t.integer :riffils_center_id
      t.integer :sector_id

      t.timestamps null: false
    end
  end
end
