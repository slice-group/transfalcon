class CreateRoadTransports < ActiveRecord::Migration
  def change
    create_table :road_transports do |t|
      t.string :code
      t.string :name
      t.integer :municipality_id
      t.string :schedule
      t.string :units
      t.string :frequency
      t.string :users_quantity
      t.string :population
      t.string :station_quantity
      t.string :timelapse
      t.string :permalink
      t.text :map

      t.timestamps null: false
    end
  end
end
