class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :mp_code
      t.string :type_payment
      t.string :transfer_code
      t.date :transfer_date
      t.string :transfer_bank
      t.belongs_to :order, index: true

      t.timestamps null: false
    end
    add_foreign_key :payments, :orders
  end
end
