class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :quantity, default: 0
      t.float :price
      t.belongs_to :user, index: true
      t.string :code
      t.string :status, default: 'Pendiente'
      t.belongs_to :product, index: true

      t.timestamps null: false
    end
    add_foreign_key :orders, :users
    add_foreign_key :orders, :products
  end
end
