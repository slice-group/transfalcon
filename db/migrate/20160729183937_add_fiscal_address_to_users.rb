class AddFiscalAddressToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fiscal_address, :string
  end
end
