Rails.application.routes.draw do

  root to: 'frontend#index'

  devise_for :users, controllers: {registrations: 'registrations'}, skip: KepplerConfiguration.skip_module_devise

  resources :admin, only: :index
  
  scope :admin do

    resources :products do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :payments do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :orders do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end
   
  	resources :users do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :road_transports do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      get ":municipality_id/sectors", action: :sectors_of_municipality, on: :collection, as: :sectors_of_municipality
    end

    resources :municipalities do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :sectors do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :terms do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :riffils_centers do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
      get ":municipality_id/sectors", action: :sectors_of_municipality, on: :collection, as: :sectors_of_municipality
    end
    
  end

  #ajax galeria
  get '/option-landing-gallery/:option', to: 'frontend#option_landing_gallery', as: :option_landing_gallery
  get '/option-show-gallery/:option', to: 'frontend#option_show_gallery', as: :option_show_gallery


  #galeria show
  get '/gallery/:option', to: 'frontend#show_gallery', as: :show_gallery

  #orders
  get '/ordenes', to: 'orders#view_orders', as: :view_orders
  post '/ordenes/new_order', to: 'orders#create_orders', as: :create_orders

  #orders edit status admin
  put '/ordenes/change_status/:id', to: 'orders#change_status', as: :change_status

  #mail
  get '/ordenes/send_receipt/:id', to: 'orders#send_receipt', as: :send_receipt

  #Mercado_Pago
  get '/ordenes/success', to: 'mercado_pago#success', as: :success
  get '/ordenes/failure', to: 'mercado_pago#failure', as: :failure
  post '/ordenes/hook', to: 'mercado_pago#hook', as: :hook

  #validar mercado pago ordenes
  get '/validate_payment/:id', to: 'orders#validate_payment', as: :validate_payment

  #user edit
  get '/profile/edit', to: 'users#edit_profile', as: :edit_profile
  put '/users/update_user_edit/:id', to: 'users#update_user_edit', as: :update_user_edit

  #export user
  get '/export/user', to: 'users#export', as: :export_user

  get '/get-sectors/:opcion/:municipality_id', to: 'frontend#get_sectors', as: :get_sectors
  get '/get-center/:opcion/:center_id', to: 'frontend#get_center', as: :get_road
  post '/get-option', to: 'frontend#get_option', as: :get_option
  get 'centros-de-recargas/:municipality_permalink/:sector_permalink/:riffils_center_permalink', to: 'frontend#show_center', as: :show_center
  get 'rutas-de-transporte/:municipality_permalink/:sector_permalink/:road_transport_permalink', to: 'frontend#show_road', as: :show_road
  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all

  #contacts
  mount KepplerContactUs::Engine, :at => '/', as: 'messages'


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'

  #blog
  mount KepplerBlog::Engine, :at => '/', as: 'blog'

  #catalogo
  mount KepplerCatalogs::Engine, :at => '/', as: 'catalogs'
end
